// karma config file - project - test AngularJs 
module.exports = function (config) {
    config.set({
        frameworks: ['jasmine'],
        plugins: [
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-jasmine',
            'karma-coverage'
        ],
        files: [
            'node_modules/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'app/scripts/*.js',
            'test/*.js'
        ],
        // start these browsers
        browsers: ['PhantomJS', 'Chrome'],
        reporters: ['progress', 'coverage'],
        preprocessors: {
            'app/scripts/*.js': ['coverage']
        },
        coverageReporter: {
            type: 'html',
            dir: 'coverage'
        },
        logLevel: config.LOG_INFO,
        singleRun: false,
        client: {
            jasmine: {
                random: false
            }
        },
        customLaunchers: {
            Chrome_without_security: {
                base: 'Chrome',
                flags: ['--disable-web-security']
            }
        }
    });
};